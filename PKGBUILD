# Maintainer: Ray Sherwin <slick517d@gmail.com>
# Contributor: Eduard Tolosa <edu4rdshl@protonmail.com>
# PKGBUILD modified from kpcyrd <kpcyrd[at]archlinux[dot]org>
# Contributor: Jean Lucas <jean@4ray.co>

# https://github.com/signalapp/Signal-Desktop/issues/6063#issuecomment-1307001166
# The official repo electron-builder-binaries has only x86-x64 built fpm binary but not arm64. Bug since 2022
# Which is why it's necessary to use USE_SYSTEM_FPM=true

# You can choose the version of Signal from the following web page:
# https://github.com/signalapp/Signal-Desktop/releases
# Please note: only stable versions are supported.

pkgname=signal-desktop
_pkgname=Signal-Desktop
pkgver=7.42.0
pkgrel=1
pkgdesc="Signal Private Messenger for Linux"
license=('GPL3')
arch=('aarch64')
url="https://signal.org"
depends=(
  'gcc-libs'
  'glibc'
  'gtk3'
  'hicolor-icon-theme'
  'libasound.so'
  'libatk-bridge-2.0.so'
  'libcairo.so'
  'libcups'
  'libdbus-1.so'
  'libdrm'
  'libexpat.so'
  'libgio-2.0.so'
  'libpango-1.0.so'
  'libx11'
  'libxcb'
  'libxcomposite'
  'libxdamage'
  'libxext'
  'libxfixes'
  'libxkbcommon.so'
  'libxrandr'
  'mesa'
  'nspr'
  'nss'
)
makedepends=(
  'git'
  'git-lfs'
  'libxcrypt-compat'
  'node-gyp'
  'nodejs'
  'npm'
  'python'
  'ruby'
)
optdepends=('xdg-desktop-portal: Screensharing with Wayland')

source=(
  "${pkgname}-${pkgver}.tar.gz::https://github.com/signalapp/${_pkgname}/archive/v${pkgver}.tar.gz"
  "${pkgname}.desktop"

)

sha256sums=('c4aaa4d3223d80dd14fc60dbfe0783b108e4ba2214d485976f7bd325490dc089'
            'bf388df4b5bbcab5559ebbf220ed4748ed21b057f24b5ff46684e3fe6e88ccce')

prepare() {

export GEM_HOME="$(gem env user_gemhome)"
export PATH="$PATH:$GEM_HOME/bin"

    # Install fpm if it is not already installed
    if ! gem list fpm -i > /dev/null; then
        echo "fpm is not installed. Installing fpm..."
        gem install fpm
        echo "fpm has been installed."
    else
        echo "fpm is already installed."
    fi
    
  cd "${_pkgname}-${pkgver}"

  # git-lfs hook needs to be installed for one of the dependencies
  git lfs install

  # Allow higher Node versions
  sed 's#"node": "#&>=#' -i package.json

  # Install dependencies for sticker-creator
  npm --prefix ./sticker-creator/ install

  # Install dependencies for signal-desktop
  npm install

}

build() {
  cd "${_pkgname}-${pkgver}"
  
# Build the sticker creator
  USE_SYSTEM_FPM=true bash -c 'npm --prefix ./sticker-creator/ run build'
 
 # Build signal-desktop
  npm run generate
  USE_SYSTEM_FPM=true bash -c 'npm run build:esbuild:prod && npm run build:release -- --linux dir'
}

package() {

  cd "${_pkgname}-${pkgver}"

  # Create the necessary directories
  install -d "${pkgdir}/usr/"{lib,bin}

  # Copy the necessary files to the destination directory
  cp -a release/linux-arm64-unpacked "${pkgdir}/usr/lib/${pkgname}"

  # Create the symbolic link
  ln -s "/usr/lib/${pkgname}/${pkgname}" "${pkgdir}/usr/bin/"

  # Apply permissions to the chrome-sandbox file
  chmod u+s "${pkgdir}/usr/lib/signal-desktop/chrome-sandbox"

  # Install the .desktop file
  install -Dm 644 "../${pkgname}.desktop" -t "${pkgdir}/usr/share/applications"

  # Install icons of various sizes
  for i in 16 24 32 48 64 128 256 512 1024; do
    install -Dm 644 "build/icons/png/${i}x${i}.png" \
      "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
  done
}
